CREATE TABLE test1_obj (
       obj_id serial PRIMARY KEY,
       obj_type varchar (50) NOT NULL,
       obj_color varchar (25) NOT NULL,
       obj_location varchar(25) check (obj_location in ('north', 'south', 'west', 'east', 'northeast', 'southeast', 'southwest', 'northwest')),
       obj_install_date date
       );